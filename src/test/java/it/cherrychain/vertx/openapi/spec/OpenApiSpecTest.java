package it.cherrychain.vertx.openapi.spec;

import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.vertx.core.Vertx.vertx;
import static io.vertx.ext.web.Router.router;
import static it.cherrychain.vertx.openapi.spec.OpenApi.openApiSpec;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.infoSpec;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.pathsSpec;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.serversSpec;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.tagsSpec;
import static it.cherrychain.vertx.openapi.spec.PathsProperty.pathItemsSpec;
import static org.assertj.core.api.Assertions.assertThat;

class OpenApiSpecTest {
  private final Router router = router(vertx());
  private final Route route = router.get("/api/:any/resource/:id").handler(it -> it.response().end());

  @Test
  @DisplayName("Should get OpenAPI instantiated correctly")
  void shouldGetOpenApiInstantiatedCorrectly() {
    final var spec = openApiSpec(
      infoSpec("prova", "sono una bellissima descrizione"),
      serversSpec(),
      tagsSpec(),
      pathsSpec(
        pathItemsSpec(
          router
        )
      )
    );

    assertThat(spec.get()).isNotNull();
    assertThat(spec.get().getInfo()).isNotNull();
    assertThat(spec.get().getPaths()).isNotNull();
    assertThat(spec.get().getServers()).isNotNull();
    assertThat(spec.get().getTags()).isNotNull();
  }

  @Test
  @DisplayName("Should have right paths")
  void shouldHaveRightPaths() {
    final var spec = openApiSpec(
      infoSpec("prova", "sono una bellissima descrizione"),
      serversSpec(),
      tagsSpec(),
      pathsSpec(
        pathItemsSpec(
          router
        )
      )
    );

    assertThat(spec.get().getPaths()).hasSize(1);
  }
}
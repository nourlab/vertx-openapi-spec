package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.Operation;
import it.cherrychain.vertx.openapi.spec.ParametersSpec;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class ParametersSpecTest {
  private final Operation operation = mock(Operation.class);

  @Test
  @DisplayName("Should set a parameter")
  void shouldSetParameter() {
    final var spec = new ParametersSpec("/api/:any/resource/:id".split("/"));

    assertThat(spec.apply(operation).get(0).getName()).isEqualTo("any");
    assertThat(spec.apply(operation).get(1).getName()).isEqualTo("id");
  }
}
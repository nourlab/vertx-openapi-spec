package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.OpenAPI;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import org.junit.jupiter.api.Test;

import static io.vertx.core.Vertx.vertx;
import static io.vertx.ext.web.Router.router;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.pathsSpec;
import static it.cherrychain.vertx.openapi.spec.PathsProperty.pathItemsSpec;
import static org.assertj.core.api.Assertions.assertThat;

class PathsSpecTest {
  private final Router router = router(vertx());
  private final Route route = router.get("/api/:any/resource/:id").handler(it -> it.response().end());

  @Test
  void shouldHavePaths() {
    final var paths = pathsSpec(pathItemsSpec(router));

    assertThat(paths.apply(new OpenAPI())).hasSize(1);
  }
}
package it.cherrychain.vertx.openapi.spec;

import io.vertx.ext.web.Route;
import it.cherrychain.vertx.openapi.spec.RouteMethodsImpl;
import org.junit.jupiter.api.Test;

import static io.vertx.core.Vertx.vertx;
import static io.vertx.core.http.HttpMethod.GET;
import static io.vertx.ext.web.Router.router;
import static org.assertj.core.api.Assertions.assertThat;

class RouteMethodsTest {
  private final Route route = router(vertx()).get("/api/:id");

  @Test
  void shouldHaveHttpMethodGet() {
    assertThat(new RouteMethodsImpl(route)).contains(GET);
  }
}
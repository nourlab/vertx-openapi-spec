package it.cherrychain.vertx.openapi.block;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

final class EmptyBlock<B> implements Block<B> {
  @Override
  public final <R> Block<R> map(Function<? super B, ? extends R> transform) {
    return new BlockImpl<>(requireNonNull(transform, "Transform can't be null.").apply(null));
  }

  @Override
  public final Block<B> peek(Consumer<? super B> peek) {
    requireNonNull(peek, "Peek can't be null.").accept(null);
    return this;
  }

  @Override
  public final Block<B> then(Runnable then) {
    requireNonNull(then, "Then can't be null.").run();
    return this;
  }

  @Override
  public final Block<B> filter(Predicate<? super B> filter) {
    return requireNonNull(filter, "Filter can't be null.").test(null)
      ? this
      : new it.cherrychain.vertx.openapi.block.EmptyBlock<>();
  }

  @Override
  public final B get() {
    return null;
  }
}

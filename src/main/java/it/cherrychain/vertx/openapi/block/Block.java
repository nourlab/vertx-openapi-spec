package it.cherrychain.vertx.openapi.block;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Objects.nonNull;

public interface Block<T> {
  static <B> Block<B> $(final B block) {
    return nonNull(block) ? new BlockImpl<>(block) : new EmptyBlock<>();
  }

  <R> Block<R> map(final Function<? super T, ? extends R> transform);
  Block<T> peek(final Consumer<? super T> peek);
  Block<T> then(final Runnable then);
  Block<T> filter(final Predicate<? super T> filter);
  T get();
}

package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.vertx.ext.web.Router;

import java.util.List;
import java.util.function.Function;

import static java.util.Arrays.copyOf;

public interface PathsProperty<T> extends Function<Paths, T> {
  static PathsProperty<PathItem> pathItemSpec(final PathSpec pathSpec, final PathItemProperty<?>... properties) {
    return new PathItemSpec(pathSpec, copyOf(properties, properties.length));
  }

  static PathsProperty<List<PathItem>> pathItemsSpec(final Router router) {
    return new PathItemsSpec(router);
  }
}

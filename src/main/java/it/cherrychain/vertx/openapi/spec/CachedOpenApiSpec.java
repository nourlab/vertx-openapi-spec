package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.OpenAPI;

import java.util.concurrent.atomic.AtomicReference;

final class CachedOpenApiSpec implements OpenApi {
  private final OpenApi openApi;
  private final AtomicReference<OpenAPI> cache;

  CachedOpenApiSpec(final OpenApi openApi) {
    this(openApi, new AtomicReference<>());
  }
  private CachedOpenApiSpec(final OpenApi openApi, final AtomicReference<OpenAPI> cache) {
    this.openApi = openApi;
    this.cache = cache;
  }

  @Override
  public final OpenAPI get() {
    cache.compareAndSet(null, openApi.get());
    return cache.get();
  }
}
